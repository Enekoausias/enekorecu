<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\PartidoModel;

/**
 * Description of PartidosController
 *
 * @author Eneko
 */
class PartidosController extends BaseController {
    //put your code here
    
    public function listaPartido()
    {
        $data['title'] = 'Lista futbol';
        $PartidoModel= new PartidoModel();
        $data['partidos']=$PartidoModel->findAll();
        return view('resultados/partidos', $data);
    }

    
    

}
