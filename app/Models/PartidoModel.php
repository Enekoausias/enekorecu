<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
namespace App\Models;
use CodeIgniter\Model;


class PartidoModel extends Model {
    protected $table = "resultados";
    protected $primaryKey ="idresultados";
    protected $returnType = "object";
    protected $allowedFields = ['equipo_ganador','equipo_perderor','resultadofinal'];
   
}