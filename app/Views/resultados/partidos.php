<!DOCTYPE html>
<html>
<head>
    <title><?=$title?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

    <h1><?=$title?> </h1>
<?php foreach ($partidos as $partido):?>
    <div class="container">
    <tr>
    
        <td>
            <?=$partido->equipo_ganador?>
        </td>
        <td>
            <?=$partido->equipo_perdedor?>
        </td>
        <td>
            <?=$partido->resultadofinal?>
        </td>
    
    </div>
</tr>
<?php endforeach; ?>

   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>